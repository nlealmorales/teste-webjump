<?php

abstract class ClassConexao{

	#Realizará a conexão com o banco de dados
	#No caso foi utilizado MySQL Server
	protected function conectaDB()
	{
		try{
			$Con = new PDO("mysql:host=localhost;dbname=webjump","root","root");
			return $Con;
		}catch (PDOException $Erro){
			return $Erro->getMessage();
		}
	}
}

?>