$(document).ready(function() {
	$("#FormCadastroProduto").on('submit',function(event){
		event.preventDefault();
		var Dados = $(this).serialize();

		$.ajax({
			url: 'Controllers/ControllerCadastro.php',
			type: 'post',
			dataType: 'html',
			data: Dados,
			success: function(Dados){
				$('.resposta').show().html(Dados);
				// setTimeout(function() {
				//       location.reload();
				// }, 2500);
			}
		});
	});

	$("#FormCadastroCategoria").on('submit',function(event){
		event.preventDefault();
		var Dados = $(this).serialize();

		$.ajax({
			url: 'Controllers/ControllerCadastro.php',
			type: 'post',
			dataType: 'html',
			data: Dados,
			success: function(Dados){
				$('.resposta').show().html(Dados);
				setTimeout(function() {
				      location.reload();
				}, 2500);

			}
		});
	});

	/* Confirmar antes de deletar os dados */
	$('.delete').on('click', function(event){
		event.preventDefault();
		var Link = $(this).attr('href');
		var obj = $(this);
		
		if(confirm("Deseja mesmo apagar esses dados?")){
			// window.location.href = Link;
			$.ajax({
				type: 'post',
				url: Link,
				cache: false,
				data: {sku:$(this).attr("sku")},
				success: function(removetr) { 
					if (removetr){
						$(obj).closest("tr").remove();
						alert("Apagado com Sucesso!");
					} else {
						alert("Impossível apagar");
					}
				}
			});
		}else{
			return false;
		}
	});
});

