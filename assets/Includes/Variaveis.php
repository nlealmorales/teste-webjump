<?php 

/* Variáveis a serem trabalhadas durante todo o projeto, podendo também ser aproveitadas por outras aplicações se necessário. */

if(isset($_POST['acao'])){
	$Acao = filter_input(INPUT_POST, 'acao', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['acao'])){
	$Acao = filter_input(INPUT_GET, 'acao', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$Acao = "";
}

if(isset($_POST['name'])){
	$Name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['name'])){
	$Name = filter_input(INPUT_GET, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$Name = "";
}

if(isset($_POST['sku'])){
	$Sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['sku'])){
	$Sku = filter_input(INPUT_GET, 'sku', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$Sku = "";
}

if(isset($_POST['sku-atual'])){
	$SkuAtual = filter_input(INPUT_POST, 'sku-atual', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['sku-atual'])){
	$SkuAtual = filter_input(INPUT_GET, 'sku-atual', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$SkuAtual = "";
}

if(isset($_POST['price'])){
	$Price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['price'])){
	$Price = filter_input(INPUT_GET, 'price', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$Price = "";
}

if(isset($_POST['quantity'])){
	$Quantity = filter_input(INPUT_POST, 'quantity', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['quantity'])){
	$Quantity = filter_input(INPUT_GET, 'quantity', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$Quantity = "";
}

if(isset($_POST['category'])){
	$Category = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['category'])){
	$Category = filter_input(INPUT_GET, 'category', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$Category = "";
}

if(isset($_POST['description'])){
	$Description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['description'])){
	$Description = filter_input(INPUT_GET, 'description', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$Description = "";
}

if(isset($_POST['category-name'])){
	$CatName = filter_input(INPUT_POST, 'category-name', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['category-name'])){
	$CatName = filter_input(INPUT_GET, 'category-name', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$CatName = "";
}

if(isset($_POST['category-code'])){
	$CatCode = filter_input(INPUT_POST, 'category-code', FILTER_SANITIZE_SPECIAL_CHARS);
}elseif (isset($GET['category-code'])){
	$CatCode = filter_input(INPUT_GET, 'category-code', FILTER_SANITIZE_SPECIAL_CHARS);
}else{
	$CatCode = "";
}

?>