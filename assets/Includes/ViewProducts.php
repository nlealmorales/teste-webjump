<div class="header-list-page">
	<h1 class="title">Products</h1>
	<a href="addProduct.php" class="btn-action">Add new Product</a>
</div>
<div class="resposta">Produto removido com sucesso!</div>
<table class="data-grid">
	<tr class="data-row">
		<th class="data-grid-th">
			<span class="data-grid-cell-content">Name</span>
		</th>
		<th class="data-grid-th">
			<span class="data-grid-cell-content">SKU</span>
		</th>
		<th class="data-grid-th">
			<span class="data-grid-cell-content">Price</span>
		</th>
		<th class="data-grid-th">
			<span class="data-grid-cell-content">Quantity</span>
		</th>
		<th class="data-grid-th">
			<span class="data-grid-cell-content">Categories</span>
		</th>

		<th class="data-grid-th">
			<span class="data-grid-cell-content">Actions</span>
		</th>
	</tr>
	<!-- Estrutura de loop que irá exibir os produtos cadastrados em nosso banco de dados -->
	<?php 
	$Crud = new ClassCrud();
	$BFetch = $Crud->selectDB(
		"*",
		"produto",
		"",
		array()
	);

	while($Fetch = $BFetch->fetch(PDO::FETCH_ASSOC)) {
	?>

	<tr class="data-row">
		<td class="data-grid-td">
			<span class="data-grid-cell-content"><?php echo $Fetch['nome']; ?></span>
		</td>

		<td class="data-grid-td">
			<span class="data-grid-cell-content"><?php echo $Fetch['sku']; ?></span>
		</td>

		<td class="data-grid-td">
			<span class="data-grid-cell-content"><?php echo 'R$'.$Fetch['preco']; ?></span>
		</td>

		<td class="data-grid-td">
			<span class="data-grid-cell-content"><?php echo $Fetch['quantidade']; ?></span>
		</td>

		<td class="data-grid-td">
			<span class="data-grid-cell-content"><?php echo $Fetch['categoria']; ?></span>
		</td>

		<td class="data-grid-td">
			<div class="actions">
				<!-- <div class="action-view"><span><a href="<?php // echo "viewitem.php?sku={$Fetch['sku']}"; ?>">View</a></span></div> -->
				<div class="action-edit"><span><a href="<?php echo "addProduct.php?sku={$Fetch['sku']}"; ?>">Edit</a></span></div>
				<div class="action-delete"><span><a class="delete" href="<?php echo "Controllers/ControllerDeletar.php?sku={$Fetch['sku']}"; ?>">Delete</a></span></div>
			</div>
		</td>
	</tr>

	<?php
	}
	?>
</table>
