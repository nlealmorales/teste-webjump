<?php 

/* Edição de categoria */
if(isset($_GET['codigo'])){

    $Acao = "EditCat";

    $Crud = new ClassCrud();
    $BFetch = $Crud->selectDB(
        "*",
        "categoria",
        "where codigo=?",
        array(
            $_GET['codigo']
        )
    );

    $Fetch = $BFetch->fetch(PDO::FETCH_ASSOC);

    $CategoryName = $Fetch['nome'];
    $CategoryCode = $Fetch['codigo'];
}

/* Novo Cadastro de categoria */
else{
    $Acao = "SaveCat";
    $CategoryName = "";
    $CategoryCode = "";
}

?>

<h1 class="title new-item">New Category</h1>
<div class="resposta"></div>
<form method="POST" id="FormCadastroCategoria" name="FormCadastroCategoria" action="Controllers/ControllerCadastro.php">
	<input type="hidden" name="acao" id="acao" value="<?php echo $Acao ?>" />
  	<div class="input-field">
	    <label for="category-name" class="label">Category Name</label>
	    <input type="text" id="category-name" name="category-name" class="input-text" value="<?php echo $CategoryName ?>" />
	    
	  </div>
	  <div class="input-field">
	    <label for="category-code" class="label">Category Code</label>
	    <input type="text" id="category-code" name="category-code" class="input-text" value="<?php echo "Inserido automaticamente." ?>" readonly/>
	  </div>
	  <div class="actions-form">
	    <a href="categories.php" class="action back">Back</a>
	    <input class="btn-submit btn-action" type="submit" value="<?php echo $Acao; ?>" />
  	</div>
</form>