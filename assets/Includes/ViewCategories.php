<div class="header-list-page">
    <h1 class="title">Categories</h1>
    <a href="addCategory.php" class="btn-action">Add new Category</a>
</div>
<table class="data-grid">
    <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
    </tr>
    <!-- Estrutura de loop que irá exibir as categorias cadastrados em nosso banco de dados -->
    <?php 
    $Crud = new ClassCrud();
    $BFetch = $Crud->selectDB(
        "*",
        "categoria",
        "",
        array()
    );

    while($Fetch = $BFetch->fetch(PDO::FETCH_ASSOC)) {
    ?> 

    <tr class="data-row">
        <td class="data-grid-td">
            <span class="data-grid-cell-content"><?php echo $Fetch['nome']; ?></span>
        </td>

        <td class="data-grid-td">
            <span class="data-grid-cell-content"><?php echo $Fetch['codigo']; ?></span>
        </td>

        <td class="data-grid-td">
            <div class="actions">
                <div class="action-edit"><span><a href="<?php echo "addCategory.php?codigo={$Fetch['codigo']}"; ?>">Edit</a></span></div>
                <div class="action-delete"><span><a class="delete" href="<?php echo "Controllers/ControllerDeletar.php?codigo={$Fetch['codigo']}"; ?>">Delete</a></span></div>
            </div>
        </td>
    </tr>

    <?php 
    }
    ?>
    
</table>