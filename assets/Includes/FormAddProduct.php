<?php 

/* Edição de produto */

if(isset($_GET['sku'])){

    $Acao = "Edit";

    $Crud = new ClassCrud();
    $BFetch = $Crud->selectDB(
        "*",
        "produto",
        "where sku=?",
        array(
            $_GET['sku']
        )
    );

    $Fetch = $BFetch->fetch(PDO::FETCH_ASSOC);

    $Name = $Fetch['nome'];
    $Sku = $Fetch['sku'];
    $Description = $Fetch['descricao'];
    $Quantity = $Fetch['quantidade'];
    $Price = $Fetch['preco'];
    $Category = $Fetch['categoria'];
}

/* Novo Cadastro de produto */
else{
    $Acao = "SaveProd";
    $Name = "";
    $Sku = "";
    $Description = "";
    $Quantity = "";
    $Price = "";
    $Category = "";
}

?>

<h1 class="title new-item">Product</h1>
<div class="resposta">Cadastro Realizado com Sucesso!</div>
<form method="POST" id="FormCadastroProduto" name="FormCadastroProduto" action="Controllers/ControllerCadastro.php">
    <input type="hidden" name="acao" id="acao" value="<?php echo $Acao ?>" />
    <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
<!-- Se for alteração de um produto cadastrado, não será possível alterar o sku. -->
<?php if($Acao=="SaveProd"){ ?>
    <input type="text" id="sku" name="sku" class="input-text" value="<?php echo $Sku; ?>"/>
<?php }else{ ?>
    <input type="text" id="sku" name="sku" class="input-text" value="<?php echo $Sku; ?>" readonly/> 
<?php } ?>     
    </div>
    <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="name" class="input-text" value="<?php echo $Name; ?>" /> 
    </div>
    <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="price" class="input-text" value="<?php echo $Price; ?>" /> 
    </div>
    <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantity" class="input-text" value="<?php echo $Quantity; ?>" /> 
    </div>
        <?php
        //if($Acao == 'Save'){
        ?>
            <div class="input-field">
                <label for="category" class="label">Categories</label>
                <select multiple id="category" name="category" class="input-text">
                <?php
                    $Crud = new ClassCrud();
                    $BFetch = $Crud->selectDB(
                        "*",
                        "categoria",
                        "",
                        array()
                    );

                    while($Fetch = $BFetch->fetch(PDO::FETCH_ASSOC)) {
                ?>      
                    <option><?php echo $Fetch['codigo'].' - '.$Fetch['nome'] ?></option>
                <?php 
                    }
                ?>
                </select>
            </div>
        <?php
        //}
        ?>
        
    <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text"><?php echo $Description; ?></textarea>
    </div>
    <div class="actions-form">
        <a href="products.php" class="action back">Back</a>
        <input type="hidden" name="sku-atual" value="<?php echo $Sku; ?>" />
        <input class="btn-submit btn-action" type="submit" value="<?php echo $Acao; ?>" />
    </div>
</form>