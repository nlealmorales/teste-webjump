<?php 
include("../Includes/Variaveis.php");
include("../Class/ClassCrud.php");

$Crud = new ClassCrud();

#Acao -> valor da variável que encontra-se dentro de um <input> no html das paginas de cadastro/edição de produtos e categorias,
#a variável irá assumir de acordo com o que for solicitado em cada página, seja para editar ou cadastrar um novo produto/categoria.
switch ($Acao) {
	case 'SaveProd':
		$Crud->insertDB(
			"produto",
			"?,?,?,?,?,?",
			array(
				$Name,
				$Sku,
				$Description,
				$Quantity,
				$Price,
				$Category		
			)
		);

		echo "Cadastro Inserido com Sucesso!";
		break;

	case 'Edit':
		$Crud->updateDB(
			"produto",
			"nome=?, sku=?, descricao=?, quantidade=?, preco=?, categoria=?",
			"sku=?",
			array(
				$Name,
				$Sku,
				$Description,
				$Quantity,
				$Price,
				$Category,
				$SkuAtual
			)
		);

		echo "Cadastro Atualizado com Sucesso!";
		break;

	case 'SaveCat':
		$null = NULL;
		$Crud->insertDB(
			"categoria",
			"?,?",
			array(
				$null,
				$CatName		
			)
		);

		echo "Categoria Inserida com Sucesso!";
		break;

	case 'EditCat':
		$Crud->updateDB(
			"categoria",
			"nome=?",
			"codigo=?",
			array(
				$CatName,
				$CatCode
			)
		);

		// var_dump($Crud);
		echo "Cadastro Atualizado com Sucesso!";
		break;
	
	default:
		echo "Nada foi feito!";
		break;
}