<?php 
include("../Includes/Variaveis.php");
include("../Class/ClassCrud.php");

$Crud = new ClassCrud();

/* Controler para utilizar a função deleteDB nas paginas de produto e categoria */

if(isset($_GET['sku'])){
	$Sku = filter_input(INPUT_GET, 'sku', FILTER_SANITIZE_SPECIAL_CHARS);

	$Crud->deleteDB(
		"produto",
		"sku=?",
		array(
			$Sku
		)
	);

	echo "Dado deletado com sucesso!";
}


if(isset($_GET['codigo'])){
	$Codigo = filter_input(INPUT_GET, 'codigo', FILTER_SANITIZE_SPECIAL_CHARS);

	$Crud->deleteDB(
		"categoria",
		"codigo=?",
		array(
			$Codigo
		)
	);

	echo "Dado deletado com sucesso!";
}
?>