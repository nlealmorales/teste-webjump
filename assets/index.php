<?php 
include("Includes/Header.php");
include("Class/ClassCrud.php");
?>

<!-- Neste body será exibido 4 produtos de forma aletória no homepage, com suas informações particulares -->
<body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      You have 
      <?php 
      $Crud = new ClassCrud();
      $BFetch = $Crud->selectDB(
        "*",
        "produto",
        "order by rand() limit 4",
        array()
      );
      // print_r($BFetch);
      $exibeHome = $BFetch->rowCount();
      $TotalFetch = $Crud->selectDB(
        "*",
        "produto",
        "",
        array()
      );
      $totalProdutos = $TotalFetch->rowCount();
      echo "<b>" . $totalProdutos . "</b>";

      ?> products added on this store: <a href="addProduct.php" class="btn-action">Add new Product</a>
    </div>
    <?php
    if($exibeHome > 0) {?>
      <ul class="product-list">
        <?php 
        $x = 0;
        $arrayProduct = array("tenis-basket-light.png", "tenis-runner-bolt.png", "tenis-sneakers-43n.png", "tenis-2d-shoes.png");
        while($Fetch = $BFetch->fetch(PDO::FETCH_ASSOC)) {
          shuffle($arrayProduct);
        //print_r($Fetch);

          $x = rand(0,3);
          ?>
          <li>
            <div class="product-image" style="text-align: center;">
        <img style="margin: 0px auto;" src="../images/product/<?php // switch ($x) {

          echo $arrayProduct[0];
          unset($arrayProduct[0]);
          ?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
        </div>
        <div class="product-info">
          <div class="product-name"><span><?php echo $Fetch['nome']; ?></span></div>
          <div class="product-price"><span class="special-price"><?php echo $Fetch['quantidade']; ?> available</span> <span><?php echo 'R$'.number_format($Fetch['preco'], 2, '.', ''); ?></span></div>
        </div>
      </li>
      <?php
    }

  } else {
    echo "Sem produtos registrados.";
  } ?>
</main>
</body>

<?php include("Includes/Footer.php"); ?>